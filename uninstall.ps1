# Local Setup

# Development packages
$devPkgs = "openjdk11", "git", "dark", "python", "vscode","7zip"

Write-Host "Cleaning Up local Development Environment"

foreach ($pkg in $devPkgs) {
    # Check if the package is installed
    $installedPackages = scoop list $pkg

    if ($installedPackages -ne $null) {
        Write-Host "Uninstalling $pkg..."
	if ($pkg -match "vscode") {
            Write-Host "Uninstalling VS Code extension: vscode-java-pack..."
            code --uninstall-extension vscjava.vscode-java-pack
        }
	
	if($pkg -match "openjdk" ){
   		$process = Get-Process | Where-Object { $_.ProcessName -like "*java*" }
   		if ($process -ne $null) {
       			Stop-Process -InputObject $process -Force -ErrorAction SilentlyContinue
   		}
	}
        
	scoop uninstall $pkg
    } else {
        Write-Host "$pkg is not installed."
    }
}

scoop uninstall scoop

Remove-Item ~\scoop -Recurse
